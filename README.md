       ______                                ____
      / ____/___  ____ ___  ____ ___  ____  / __ \
     / /   / __ \/ __ `__ \/ __ `__ \/ __ \/ / / /
    / /___/ /_/ / / / / / / / / / / / / / / /_/ /
    \____/\____/_/ /_/ /_/_/ /_/ /_/_/ /_/\___\_\


# CommnQ.js
## CommnQ implementation for Node.js

CommnQ was originally a Python script for monitoring GLUE2 AMQP messages
for TACC's resource monitoring and information framework.  It was since
ported to node.js and the handler mechanism abstracted.  Three handlers
are implemented as useful extension points for developing customized
message handlers.

CommnQ.js is build on top of the [node-amqp](https://github.com/postwait/node-amqp)
by @postwait.

## Documentation

### Setting up a CommnQ server

Setting up a server is easy! Just create a new `commnq` object and pass it
and object with connection parameters for the AMQP host.  These parameters
are in the same format as for node-amqp.  Finally, call `connect(callback)`
where callback is a function that will be executed after the AMQP handshake
has completed.


    var Commnq = require('commnq');

    var readyCallback = function() {
      console.log('CommnQ ready!');
    };

    var server = new Commnq({
      host: 'amqp.example.com'
      , port: 5672
      , login: 'guest'
      , password: 'guest'
      });

    server.connect(readyCallback);


### Registering a handler

You should register you handlers inside the ready callback for the CommnQ
connection.


    var server = new Commnq(params);
    server.connect(function() {
      server.registerHandler(new Commnq.Handlers.DefaultHandler({
        exchange: 'default'
      }));
    });


You can see an example custom handler in `example/example.js`.


