/**
 * file: TaskHandler.js
 * A Commnq-js handler which takes an ordered array of task
 * functions. Each message that is received is passed to
 * each task function in order with the signature:
 * function(message, originalMessage, deliveryInfo, headers) {}
 *
 * Author: Matthew R Hanlon <mrhanlon@tacc.utexas.edu>
 */

'use strict';

var _ = require('underscore');

var TaskHandler = function(options, tasks) {
  if (! (this instanceof TaskHandler)) {
    return new TaskHandler(options, tasks);
  }

  this.exchange = options.exchange;
  this.routingKey = options.routingKey || '#';
  this.options = options;
  this.tasks = tasks;

  _.bindAll(this, 'queueName', 'onMessage');
};

TaskHandler.prototype.queueName = function() {
  return 'TaskHandler' + new Date().getTime() + '#' + this.exchange;
};

TaskHandler.prototype.queueOptions = function() {
  return {
    exclusive: true
  };
};

TaskHandler.prototype.onMessage = function(message, headers, deliveryInfo) {
  // parse message data buffer to JSON
  try {
    var originalMessage, processedMessage;

    if (Buffer.isBuffer(message.data)) {
      originalMessage = JSON.parse(message.data.toString());
    } else {
      originalMessage = message.data;
    }

    processedMessage = originalMessage;

    // execute tasks
    _.each(this.tasks, function(task) {
      processedMessage = task(processedMessage, originalMessage, deliveryInfo, headers);
    });
  } catch (err) {
    console.log(err);
  }
};

module.exports = TaskHandler;
