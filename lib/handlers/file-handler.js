/**
 * A basic handler that writes messages as json documents to the filesystem.
 *
 */

'use strict';

var fs = require('fs-extra')
  , path = require('path')
  , lockFile = require('lockfile')
  , util = require('util')
  , _ = require('underscore')
  , DefaultHandler = require('./default-handler');

var FileHandler = function(options) {
  if (! (this instanceof FileHandler)) {
    return new FileHandler(options);
  }
  this.outputPath = options.outputPath;
  DefaultHandler.call(this, options);
};

util.inherits(FileHandler, DefaultHandler);

/*
 * The outputPath can contain replacement patterns based on properties in the message.
 * This allows the output file to be customized based on the specific message that is
 * received.  Specify this by: '/path/to/{fooProperty}/{barProperty}/file'
 */
FileHandler.prototype.makePath = function(message) {
  return _.reduce(message, function(memo, val, key) {
    return memo.replace('{' + key + '}', val);
  }, this.outputPath);
};

FileHandler.prototype.processMessage = function(message) {
  var outputPath = this.makePath(message)
    , handler = this;

  // make sure directory exists
  if (! fs.existsSync(path.dirname(outputPath))) {
    fs.mkdirsSync(path.dirname(outputPath));
  }

  // Lock the output path to prevent clobbering.  The lock is retried 5 times every 5s.
  // Locks older than 5 minutes are considered stale.
  lockFile.lock(outputPath + '.lock', {retries: 5, retryWait: 5000, stale: 300000}, function(err) {

    if (err) {
      // error obtaining lock
      throw err;
    }

    handler.writeOutputFile(outputPath, message);

    // make sure to unlock file!
    lockFile.unlock(outputPath + '.lock', function(err) {
      if (err) {
        throw err;
      }
    });
  });
};

FileHandler.prototype.writeOutputFile = function(outputPath, message) {
  var outputData = this.preprocessOutput(outputPath, message);
  fs.outputJson(outputPath, outputData);
};

FileHandler.prototype.preprocessOutput = function(outputPath, message) {
  return message;
};

module.exports = FileHandler;