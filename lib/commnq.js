/*
 * commnq-js
 * https://bitbucket.org/taccaci/commnq-js
 *
 * Copyright (c) 2013 Matthew R Hanlon
 * Licensed under the MIT license.
 */

'use strict';

var amqp = require('amqp');

var Commnq = function(options) {
  var config = options, connection = null;

  return {
    getConnection: function() {
      return connection;
    },

    getConfig: function() {
      return config;
    },

    connect: function(readyHandler) {
      if (connection !== null) {
        console.log('Attempt to connect to AMQP server while already connected.');
      } else {
        connection = amqp.createConnection(config);
        if (typeof readyHandler === "function") {
          connection.on('ready', readyHandler);
        }
      }
      return this;
    },

    disconnect: function() {
      if (connection === null) {
        console.log('Attempt to disconnect to AMQP server but no connection exists.');
      } else {
        connection.disconnect();
        connection = null;
      }
      return this;
    },

    registerHandler: function(handler) {
      if (connection === null) {
        console.log('Attempt to register handler for AMQP server, but no connection is available.');
      } else {
        connection.queue('', handler.queueOptions(), function(queue) {
          queue.bind(handler.exchange, handler.routingKey);
          queue.subscribe(handler.onMessage);
        });
      }
      return this;
    }
  };
};

/**
 * These are the default library handlers and are good extension points for custom handlers.
 */
Commnq.Handlers = {};
Commnq.Handlers.DefaultHandler = require('./handlers/default-handler');
Commnq.Handlers.FileHandler = require('./handlers/file-handler');
Commnq.Handlers.HttpHandler = require('./handlers/http-handler');
Commnq.Handlers.TaskHandler = require('./handlers/task-handler');

module.exports = Commnq;
